default: opa-example

clean:
	-rm -f $(XML_FILE) $(JSON_FILE)

clean-opa:
	-rm -f opa

clean-all: clean clean-opa

opa-example: opa
	./opa eval --fail-defined -i input.json -d example.rego "data.example.violation[x]" 
	@echo SUCCESS

download-opa: opa
opa:
	curl -L -o opa https://openpolicyagent.org/downloads/v0.41.0/opa_linux_amd64_static

install-requirements:
	pip install -r requirements.txt

PAGE_ID=
CONFLUENCE_SERVER=https://usedeskkb.atlassian.net/wiki
CONFLUENCE_API=$(CONFLUENCE_SERVER)/rest/api/content/$(PAGE_ID)?expand=body.storage
PARSE_RESPONSE=jq --raw-output '[.results[].body.storage.value] | join("")'
XML_HEADER=<!DOCTYPE xml SYSTEM "xhtml.ent" []><xml xmlns:atlassian-content="http://atlassian.com/content" xmlns:ac="http://atlassian.com/content" xmlns:ri="http://atlassian.com/resource/identifier" xmlns:atlassian-template="http://atlassian.com/template" xmlns:at="http://atlassian.com/template" xmlns="http://www.w3.org/1999/xhtml">
XML_FOOTER=</xml>
XML_FILE=response_body.xml
JSON_FILE=response_body.json

confluence-to-json: $(JSON_FILE)

$(JSON_FILE):
	@echo '$(XML_HEADER)' > $(XML_FILE)
	curl $(CONFLUENCE_API) | $(PARSE_RESPONSE) >> $(XML_FILE)
	@echo '$(XML_FOOTER)' >> $(XML_FILE)
	cat $(XML_FILE) | xq > $(JSON_FILE)

DOCKER_TAG=confluence-opa-validator
docker-build:
	docker build -t $(DOCKER_TAG) .

docker-run:
	docker run --rm -it $(DOCKER_TAG)
