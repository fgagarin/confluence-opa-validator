FROM python:3

RUN set -ex; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		jq \
    ; \
	rm -rf /var/lib/apt/lists/*

ADD https://openpolicyagent.org/downloads/v0.41.0/opa_linux_amd64_static /usr/bin/opa
RUN chmod +x /usr/bin/opa

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY entrypoint.sh ./
RUN chmod +x entrypoint.sh

ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
