#!/bin/bash
set -e
Help()
{
   # Display Help
   echo "Validate confluence page using rego rules."
   echo
   echo "Syntax: scriptTemplate [-p id|h|v|s url|d path|r name|i name]"
   echo "options:"
   echo "p     Confluence page id."
   echo "s     Confluence server url."
   echo "d     Data directory."
   echo "i     Input json file."
   echo "r     Rego rules file."
   echo "h     Print this Help."
   echo "v     Verbose mode."
   echo
}

GetConfluencePageAsJson()
{
   # PAGE_ID=
   CONFLUENCE_SERVER=${CONFLUENCE_SERVER:-https://usedeskkb.atlassian.net/wiki}
   API_PAGE=$CONFLUENCE_SERVER/rest/api/content/$PAGE_ID?expand=body.storage
   DATA_DIR=${DATA_DIR:-.}
   XML_HEADER='<!DOCTYPE xml SYSTEM "xhtml.ent" []><xml xmlns:atlassian-content="http://atlassian.com/content" xmlns:ac="http://atlassian.com/content" xmlns:ri="http://atlassian.com/resource/identifier" xmlns:atlassian-template="http://atlassian.com/template" xmlns:at="http://atlassian.com/template" xmlns="http://www.w3.org/1999/xhtml">'
   XML_FOOTER='</xml>'
   XML_FILE=$DATA_DIR/response_body.xml
   JSON_FILE=$DATA_DIR/response_body.json

	echo $XML_HEADER > $XML_FILE
	curl -s $API_PAGE | jq --raw-output '[ if .results then .results else [.] end | .[].body.storage.value] | join("")' >> $XML_FILE
	echo $XML_FOOTER >> $XML_FILE
	cat $XML_FILE | xq > $JSON_FILE
}

RunOPA()
{
   DATA_DIR=${DATA_DIR:-.}
   INPUT=$DATA_DIR/${INPUT:-response_body.json}
   DATA=$DATA_DIR/${RULES:-example.rego}
   EXPRESSION="data.example.violation[x]"
   if [ ! -z ${VERBOSE+x} ]; then
      echo "$DATA:"
      cat "$DATA"
      echo "$INPUT:"
      cat "$INPUT"
   fi
   OPA=`which opa || echo ./opa`
   $OPA eval --fail-defined -i "$INPUT" -d "$DATA" "$EXPRESSION"
   echo SUCCESS
}

# Get the options
while getopts ":hvp:s:d:r:i:" option; do
   case $option in
      i) # input json
         INPUT=$OPTARG;;
      r) # rules file
         RULES=$OPTARG;;
      d) # data directory
         DATA_DIR=$OPTARG;;
      s) # server url
         CONFLUENCE_SERVER=$OPTARG;;
      v)
         VERBOSE=true;;
      p) # Enter a page id
         PAGE_ID=$OPTARG;;
      h) # display Help
         Help
         exit;;
      \?) # Invalid option
         echo "Error: Invalid option"
         exit;;
   esac
done

if [ ! -z ${VERBOSE+x} ]; then
   set -x
   echo "PAGE_ID=$PAGE_ID"
fi

GetConfluencePageAsJson
RunOPA


